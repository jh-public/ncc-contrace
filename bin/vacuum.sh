#!/usr/bin/env bash
full=
while getopts 'f' opt; do
    case $opt in
        f) full=' full ';;
    esac
done
shift $((OPTIND-1))

date
time docker-compose exec -u postgres db psql -c "vacuum $full verbose analyze;"
date
