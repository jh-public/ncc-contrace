#!/usr/bin/env bash
docker-compose exec db sed -i "/max_connections/s/${2:-100}/${1:-250}/" /var/lib/postgresql/data/postgresql.conf
echo -n "Restart service? (ctrl-C to abort) "
read x
docker-compose restart db
