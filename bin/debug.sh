#!/usr/bin/env bash
ENTRYPOINT=" --entrypoint bash"
PORT=8001
while getopts 'e:p:' opt; do
    case $opt in
        e) ENTRYPOINT=" --entrypoint $OPTARG";;
        p) PORT=$OPTARG;;
    esac
done
shift $((OPTIND-1))

run-gae(){
    docker run -it --rm \
        -v /google_appengine:/google_appengine \
        -v /google_appengine/data-debug:/google_appengine/data \
        -v $PWD:/code \
        --network ${project}_default \
        -p 8080 \
        --name ${project}_debug \
        $ENTRYPOINT \
        $image
}

run-django(){
set -x
    docker run -it --rm \
        -v $PWD:/code \
         -v /home:/code/home \
        --link ${project}_db_1 \
        --network ${project}_default \
        -p ${PORT}:8001 \
        --name ${project}_debug \
        $ENTRYPOINT \
        $image
}

open-browser(){
    container=${project}_debug
    for i in $( seq 20 ); do
        if port="$( docker port $container 2>&1)"; then
            break
        fi
        sleep 1
    done
    port=$( echo $port | cut -d: -f2 )
    echo $port
#    which xdg-open && xdg-open http://localhost:$port/static/www/index.html
}

main(){
    service=${1-app}
    project=$( basename $PWD )
    image=huanjason/django:2.2

    open-browser &

    if [[ -e app.yaml ]]; then
        run-gae
    else
        run-django
    fi
}
main "$@"
