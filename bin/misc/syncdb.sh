#!/usr/bin/env bash
set -e

dumponly=false
loadonly=false
while getopts 'dl' opt; do
  case $opt in
    d) dumponly=true;;
    l) loadonly=true;;
  esac
done
shift $((OPTIND-1))

dumpdata(){
  echo Dumping data to data.json...
  local server="${1:-47}"
  local dir="${2:-sics}"
  shift 2 || true
  ssh $server "
  cd \"${dir}\" || exit 1
  bin/exec django/manage.py dumpdata \
    --natural-primary \
    --natural-foreign \
    --indent 4  \
    --exclude contenttypes \
    --exclude auth.Permission \
    $* \
  ">data.json
}

loaddata(){
  ./.e ./manage.py loaddata -v3 data.json
}

main(){
  if $dumponly; then
    dumpdata  $*
    exit
  fi

  if $loadonly; then
    loaddata  $*
    exit
  fi

  dumpdata  "$@"
  loaddata  "$@"
  rm data.json
}
main "$@"
