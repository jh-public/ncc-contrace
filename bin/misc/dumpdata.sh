#!/usr/bin/env bash
backup_dir="$( dirname $( dirname $( readlink -f $0 ) ) )/bak"
mkdir -p "$backup_dir"
backup_file="dumpdata_$( date +%y%m%d-%H%M ).json"

while getopts 'f:' opt; do
  case $opt in
    f) backup_file="$OPTARG";;
  esac
done
shift $((OPTIND-1))

/usr/local/bin/docker-compose exec -T app django/manage.py dumpdata \
    --natural-primary \
    --natural-foreign \
    --indent 4  \
    --exclude contenttypes \
    --exclude auth.Permission \
    $* \
  >"$backup_dir/$backup_file"

