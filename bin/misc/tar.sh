#!/usr/bin/env bash
date

dir="$( dirname $( dirname $( readlink -f $0 ) ) )"
mkdir -p "$dir"/bak
cd "$dir" || exit 1

backup_file="${1:-"$dir"/bak/tar_$( date +%y%m%d-%H%M ).tgz}"
tar czf "$backup_file" data static/media

owner="$(echo $PWD | cut -d / -f 3 )"
chown $owner "$backup_file"
