#!/usr/bin/env bash
server=${1:-47}

find-tar(){
    ssh $server "ls -1 sics/bak/tar* | tail -n1"
}

untar(){
    local filename=${1:?}
    ssh $server cat $filename | sudo tar xzv
}

main(){
    echo Copying data from server $server...
    untar "$( find-tar )"
}
main "$@"
