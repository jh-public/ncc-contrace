[[ -e docker-compose.yml ]] || exit 1
sudo -v
dc down
sudo rm -rf data
dc up -d
dc logs -f
./.m makemigrations project
./.migrate
time ./.m load_initial_data
