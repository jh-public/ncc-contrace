#!/usr/bin/env bash
dir="$( dirname $( dirname $( readlink -f $0 ) ) )"
mkdir -p "$dir"/bak
backup_file="${1:-"$dir"/bak/pg_dump_$( date +%y%m%d-%H%M ).txt}"
/usr/local/bin/docker-compose exec -Tu postgres db \
    pg_dump \
  >"$backup_file"

