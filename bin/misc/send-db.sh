#!/usr/bin/env bash
[[ -d data ]] || { echo data directory not found; exit 1; }
host=${1:?}
sudo -v
docker-compose stop
time sudo tar -hScf -  data | pv | ssh $host "cat >imomdb-$( date +%Y%m%d ).tar"
docker-compose up -d
