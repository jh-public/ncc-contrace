#!/usr/bin/env bash
[[ -d data ]] || { echo Directory not found: ./data; exit 1; }
tarfile=${1:?}
[[ -f $tarfile ]] || { echo tar file not found: $tarfile; exit 2; }

sudo -v
docker-compose stop
sudo rm -rf data
time sudo pv $tarfile | tar xf -
docker-compose up -d
