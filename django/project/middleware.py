
class ContentSecurityPolicyMiddleware:

    def __init__( self, get_response ):
        self.get_response = get_response

    def __call__( self, request ):
        host = request.META ['HTTP_HOST']
        response = self.get_response( request )
        response ['Content-Security-Policy-Report-Only'] = (
            'default-src '
                + "'self' "
                + "'unsafe-inline' "
                + 'cdnjs.cloudflare.com '
            + f'; report-uri http://{host}/csp/ '
            + '; img-src * data:'
        )
        return response
