from django.conf                    import settings
from django.urls                    import path, include

def add_djdt( urlpatterns, prefix='__debug__/' ):
    if settings.DEBUG:
        import debug_toolbar
        urlpatterns = [
            path( prefix, include (debug_toolbar.urls ) ),
        ] + urlpatterns

    return urlpatterns

