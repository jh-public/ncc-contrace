
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'contrace',
    'project',
    'rest_framework',
    'debug_toolbar',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': '_postgres_',
        'HOST': 'db',
    },
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'project.middleware.ContentSecurityPolicyMiddleware',
]

TIME_ZONE = 'Asia/Singapore'
USE_THOUSAND_SEPARATOR = True


# Static files (CSS, JavaScript, Images)
STATIC_URL  = '/static/'
STATIC_ROOT = os.path.join( BASE_DIR, 'static' )
MEDIA_URL   = '/media/'
MEDIA_ROOT  = os.path.join( BASE_DIR, 'media' )


# E-mail
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
try: from .local_settings_email import *
except ModuleNotFoundError: pass

# debug toolbar
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK" : lambda request: DEBUG and request.user.is_superuser,
}

# django rest framework
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
}


# LDAP
# AUTHENTICATION_BACKENDS = [
#     'project.ldapbackend.AStarLDAPBackend',
#     'django.contrib.auth.backends.ModelBackend',
# ]

