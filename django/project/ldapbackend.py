from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models   import User

import ldap
import logging


class LDAPBackend( ModelBackend ):

    LDAP_SERVER = None

    def authenticate( self, request, username=None, password=None ):
        raise NotImplementedError


    def authenticate_ldap( self, username=None, password=None ):
        if not username or not password:
            return
        conn = self.simple_bind( username, password )
        if not conn:
            return
        return conn.whoami_s()


    def get_connection( self ):
        assert self.LDAP_SERVER is not None, 'LDAP server not configured'
        conn=ldap.initialize('ldap://' + self.LDAP_SERVER )
        conn.protocol_version = 3
        conn.set_option( ldap.OPT_REFERRALS, 0)
        conn.set_option( ldap.OPT_NETWORK_TIMEOUT, 2 )
        return conn


    def simple_bind( self, username, password ):
        conn = self.conn = self.get_connection()
        try:
            result = conn.simple_bind_s(username, password)
            return conn
        except ldap.SERVER_DOWN:
            return
        except ldap.INVALID_CREDENTIALS:
            logging.error( 'Invalid credentials' )
        except ldap.SERVER_DOWN as e:
            logging.error( e )



class AStarLDAPBackend( LDAPBackend ):

    AUTO_REGISTER_URL_FRAGMENTS = [
        '/?next=/questionnaire/ldap/',
    ]
    AUTO_REGISTER_USER  = False
    BASE_DN             = "dc=ares,dc=shared-svc,dc=local"
    DEFAULT_DOMAIN      = 'ARES'
    LDAP_SERVER         = '10.217.126.85'

    def authenticate( self, request, username=None, password=None ):

        if username.upper().startswith( '%s.' % self.DEFAULT_DOMAIN ) or username == 'admin':
            return

        if username and username.find( '\\' ) == -1:
            username = '%s\\%s' % ( self.DEFAULT_DOMAIN, username )

        ldapname = self.authenticate_ldap( username, password )
        if not ldapname:
            return

        if ldapname.startswith( 'u:' ):
            ldapname = ldapname [2:]
        ldapname = ldapname.replace( '\\', '.' )

        try:
            user = User.objects.get( username__iexact=ldapname )
        except User.DoesNotExist:
            user = self.auto_register_user( request, ldapname )
            if not user:
                logging.error( f'User {ldapname} not in DB' )
                return

        user = self.update_email( user )
        return user


    def auto_register_user( self, request, username ):
        http_referer = request.META ['HTTP_REFERER']

        if self.AUTO_REGISTER_USER:
            return self.create_user( username )

        for fragment in self.AUTO_REGISTER_URL_FRAGMENTS:
            if fragment in http_referer:
                return self.create_user( username )


    def create_user( self, username ):
        user = User.objects.create( username = username )
        AutoRegisteredUser.objects.create( user = user )
        return user


    def search( self,
        filters,
        attrlist="*",
        baseDN=None,
        searchScope=ldap.SCOPE_SUBTREE,
    ):

        def get_user_info( data ):
            data = [ (k, vals ) for k, vals in data if k ]
            if len( data ) != 1:
                raise ValueError( 'Invalid LDAP info' )
            key, vals = data [0]
            vals = { k: v[0].decode() for k,v in vals.items() }
            vals ['_key'] = key
            return vals

        if not isinstance( attrlist, list ):
            attrlist = [ attrlist ]
        try:
            data = self.conn.search_s(
                baseDN or self.BASE_DN,
                searchScope,
                filters,
                attrlist,
            )
        except ldap.LDAPError as error:
            logging.error( error )
            return
        return get_user_info( data )


    def update_email( self, user ):
        userid = user.username.replace( '%s.' % self.DEFAULT_DOMAIN, '' )
        filters = '(sAMAccountName=%s)' % userid
        attrs  = [ 'cn', 'givenName', 'mail', 'sn' ]
        ldapinfo = self.search( filters, attrs )
        if ldapinfo ['mail'] != user.email:
            user.email      = ldapinfo ['mail']
            user.first_name = ldapinfo ['givenName']
            user.last_name  = ldapinfo ['sn']
            user.save()
        return user
