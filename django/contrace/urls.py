from django.urls import path, include
from .views      import *

urlpatterns = [
    path( '<int:venue_id>/submitted/',  Submitted.as_view(),    name='submitted' ),
    path( '<int:venue_id>/',            Attendance.as_view(),   name='attendance' ),
    path( '',                           Index.as_view(),        name='index' ),
]
