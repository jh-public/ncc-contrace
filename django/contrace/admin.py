from django.contrib     import admin
from contrace.models    import *


@admin.register( Attendance )
class AttendanceAdmin( admin.ModelAdmin ):
    list_display    = [ 'venue', 'date_created' ]
    list_filter     = [ 'venue' ]
    ordering        = [ '-date_created' ]
    date_hierarchy  = 'date_created'


admin.site.register( Venue )

