
var app = new Vue({

  el: '#app',

  computed:{

    submitDisabled: function(){
      return !( this.model.name.trim() && this.model.tel.trim() );
    },

  },

  data: {
    model: {
      name: '',
      tel:  '',
    },
    data: '',
  },

  methods:{

    submit: function(){
      if ( !this.model.name.trim() || !this.model.tel.trim() ){
        return;
      }
      this.data = JSON.stringify( this.model );
      this.$nextTick( () => {
        document.forms [0].submit();
      });

    },

  },

});
