from django.contrib.postgres.fields     import JSONField
from django.db                          import models


class BaseModel( models.Model ):
    date_created    = models.DateTimeField( auto_now_add = True )
    date_modified   = models.DateTimeField( auto_now = True )

    class Meta:
        abstract = True


class Attendance( BaseModel ):
    venue       = models.ForeignKey( 'Venue', on_delete=models.PROTECT )
    data        = JSONField()

    class Meta:
        verbose_name_plural = 'Attendance'

    def __str__( self ):
        return f'{self.venue}::{self.id} ({self.date_created})'


class Venue( BaseModel ):
    name = models.CharField( max_length=128, unique=True )

    def __str__( self ):
        return self.name

