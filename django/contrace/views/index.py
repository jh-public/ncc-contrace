from django.shortcuts   import render
from django.views       import View
from ..                 import models


class Index( View ):

    def get( self, request ):
        title  = 'Contact Tracing'
        venues = models.Venue.objects.values( 'id', 'name' )
        return render( request, 'index/index.html', locals() )
