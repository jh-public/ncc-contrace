from django.shortcuts   import render
from django.views       import View
from psalm91            import psalm_91
import random

class Submitted( View ):

    def get( self, request, venue_id ):
        title  = 'Contact Tracing'
        no = random.randrange( 1, len( psalm_91 ) ) - 1
        verse = psalm_91 [no]
        return render( request, 'attendance/submitted.html', locals() )
