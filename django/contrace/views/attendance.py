from django.http        import HttpResponseRedirect, HttpResponse
from django.shortcuts   import render
from django.views       import View
from ..                 import models
import json

class Attendance( View ):

    def get( self, request, venue_id ):
        title  = 'Contact Tracing'
        venue = models.Venue.objects.get( id = venue_id )
        return render( request, 'attendance/index.html', locals() )

    def post( self, request, venue_id ):
        data = request.POST ['data']
        models.Attendance.objects.create(
            venue_id = venue_id,
            data = json.loads( data ),
        )
        return HttpResponseRedirect( 'submitted/'  )
